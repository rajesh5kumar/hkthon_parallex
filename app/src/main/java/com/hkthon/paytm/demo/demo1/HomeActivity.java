package com.hkthon.paytm.demo.demo1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.hkthon.paytm.demo.MainActivity;
import com.hkthon.paytm.demo.R;

/**
 * Created by rajesh5kumar on 20/1/16.
 */
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    Button button1;
    Button button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        button1 = (Button) findViewById(R.id.b1);
        button1.setOnClickListener(this);
        button2 = (Button) findViewById(R.id.b2);
        button2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Class<?> clazz = null;

        if(v.getId() == R.id.b1) {
            clazz = WhatsappLikeScreen.class;
        }

        if(v.getId() == R.id.b2) {
            clazz = MainActivity.class;
        }

        if(clazz != null) {
            Intent intent = new Intent(this, clazz);
            startActivity(intent);
        }
    }
}
