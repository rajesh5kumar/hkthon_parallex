package com.hkthon.paytm.demo;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    AppBarLayout appBarLayout;

    CollapsingToolbarLayout collapsingToolbarLayout;

    Toolbar toolbar;

    HeaderView toolbarHeaderView;

    HeaderView floatHeaderView;

    TextView txt_paytmAboutus;

    private boolean isHideToolbarView = false;

    private String paytmAboutUs="Paytm is India’s largest mobile commerce platform. Paytm started by offering mobile recharge and utility bill payments and today it offers a full marketplace to consumers on its mobile apps. We have over 100mn registered users. In a short span of time Paytm has scaled to more than 60 Million orders per month.Paytm is the consumer brand of India’s leading mobile internet company One97 Communications. One97 investors include Ant Financial (AliPay), SAIF Partners, Sapphire Venture and Silicon Valley Bank.We strive to maintain an open culture where everyone is a hands-on contributor and feels comfortable sharing ideas and opinions. Our team spends hours designing each new feature and obsesses about the smallest of details.Our approach is simple – to design something we’d LOVE to use ourselves. Therefore we listen and take the time to understand our users and take their reactions most seriously.\n" +
            "Making stuff easy and intuitive is not our only goal. In addition to usability, we strive to create accessibility, convenience and credibility. Simplicity reflects in our home page design and this mantra has been followed throughout the site and our apps.\n" +
            "The world is increasingly mobile: people want access from wherever they are, whenever they need it. At Paytm, you have the option of recharging and shopping from whenever, anywhere and are equipped with a secure online wallet called Paytm Cash.\n" +
            "At team Paytm we set high goals and achieve them. And it’s all to provide YOU, the user, an experience that’s nothing short of awesome!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ButterKnife.bind(this);

        appBarLayout = (AppBarLayout)findViewById(R.id.app_bar_layout);
        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbarHeaderView = (HeaderView)findViewById(R.id.toolbar_header_view);
        floatHeaderView = (HeaderView)findViewById(R.id.float_header_view);
        txt_paytmAboutus = (TextView)findViewById(R.id.txt_paytmAboutus);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout.setTitle(" ");

        toolbarHeaderView.bindTo("Ramachandran", "last seen : today");
        floatHeaderView.bindTo("Ramachandran", "last seen : today");
        txt_paytmAboutus.setText(paytmAboutUs);

        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {

        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        if (percentage >= 1.0f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

}
